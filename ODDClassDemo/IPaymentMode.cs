﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDClassDemo
{
    interface IPaymentMode //Interface has been created
    {
        //Make Payment
        void MakePayment(); //No implementation here....
    }
}
