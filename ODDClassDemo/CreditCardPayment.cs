﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDClassDemo
{
    class CreditCardPayment : IPaymentMode
    {
        //For the credit card payment
        public void MakePayment()
        {
            //Action?
            Console.WriteLine("Credit card payment mode activated....");
        }

    }
}
