﻿using System;

namespace ODDClassDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create a Payment Application
            //This is an example application for tightly coupled...

            //Interface = Readability + Reusability = Low Coupling

            /* Type of Payment
             * Credit Card Payment - class created
             * Debit Card Payment - anoterh class required for the debit card
             * 
             * +
             * Add one more payment method (Google Pay)
             */

            //CreditCardPayment creditCardPayment = new CreditCardPayment();// first object created
            //DebitCardPayment debitCardPayment = new DebitCardPayment();

            //GooglePay googlePay = new GooglePay(); //** Modifed here


            ////object is created for the constructor
            //PaymentManager paymentManager = new PaymentManager(creditCardPayment, debitCardPayment, googlePay);//** Modifed here

            //paymentManager.ManagePayment(); //Call the Manage Payment method

            //Below is the example for the loose couplling...

            var creditCard = PaymentModeFactory.Create(PaymentModeFactory.Mode.GooglePay);
            PaymentManager paymentManager = new PaymentManager(creditCard);

            paymentManager.ManagePayment();

            Console.ReadKey();
        }
    }
}
