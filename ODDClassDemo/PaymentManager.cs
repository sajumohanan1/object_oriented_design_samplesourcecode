﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDClassDemo
{
    class PaymentManager
    {
        /* Codes for Tight Coupling - which is the bad practice*/
        ////To handle the payment - 
        //private readonly CreditCardPayment creditCard; 
        //private readonly DebitCardPayment debitCard;
        //private GooglePay googlePay; //** Modified codes here

        ////constructor created with two parameters
        //public PaymentManager(CreditCardPayment creditCard, DebitCardPayment debitCard, GooglePay googlePay)
        ////** Modified codes here
        //{
        //    this.creditCard = creditCard;
        //    this.debitCard = debitCard;
        //    this.googlePay = googlePay; //** Modified codes here
        //}
        ////To select the payment
        //public void ManagePayment()
        //{
        //  /creditCard.MakePayment(); //Invoking MakePayment method from Credit Card class
        //  debitCard.MakePayment();
        //    googlePay.MakePayment(); //** Modified codes here


        readonly IPaymentMode paymentMode;

        public PaymentManager(IPaymentMode paymentMode)
        {
            this.paymentMode = paymentMode;
        }
        public void ManagePayment()
        {
            paymentMode.MakePayment();
        }
    
    }
}
