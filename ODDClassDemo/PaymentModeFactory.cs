﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODDClassDemo
{
    class PaymentModeFactory
    {
        //List of payments  - enum
        public enum Mode
        {
            CreditCard,
            DebitCard,
            GooglePay
        }
        
        public static IPaymentMode Create(Mode mode)
        {
            if(mode==Mode.CreditCard)
            {
                return new CreditCardPayment();
            }
            else if(mode == Mode.DebitCard)
            {
                return new DebitCardPayment();
            }
            else
            {
                return new GooglePay();
            }
        }
    }
}
